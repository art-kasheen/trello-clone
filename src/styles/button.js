import styled from 'styled-components'

export const Button = styled.button`
  text-decoration: none;
  height: 30px;
  line-height: 30px;
  padding: 0 20px;
  border-radius: 5px;
  border: 0;
  background: ${(props) => (props.dark ? '#B5BBC5' : '#f0f2f5')};
  color: ${(props) => (props.dark ? '#F1F2F4' : '#5b6a82')};
  font-size: 13px;
  text-align: center;
  min-width: ${(props) => (props.width && props.width) || '100px'};
  display: inline-block;
  cursor: pointer;
  transition: opacity 0.2s ease-out;

  :disabled {
    opacity: 0.6;
    cursor: not-allowed;
  }

  :hover {
    opacity: 0.85;
  }

  :focus {
    outline: 0;
    box-shadow: 0 0 0 2px rgba(0, 0, 0, 0.1);
  }
`
