import styled from 'styled-components'

export const Container = styled.section`
  max-width: 900px;
  padding: 0 15px;
  margin: 0 auto;
  display: flex;
`
