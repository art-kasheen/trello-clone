import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
	* {
		box-sizing: border-box;
	}
	body {
		margin: 0;
		font-family: 'Montserrat', sans-serif;
		overflow-x: hidden;
	}
`
