import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { boardSelector, listsSelector } from '../selectors'
import Lists from '../components/List/Lists'
import Loader from '../components/Loader'
import Error from '../components/Error'
import styled from 'styled-components'
import { DragDropContext } from 'react-beautiful-dnd'
import { reorderLists, reorderCards, pushCard, removeCard } from '../actions'

const Board = ({
  board,
  lists,
  reorderLists,
  reorderCards,
  pushCard,
  removeCard,
  loading,
  loaded
}) => {
  if (loading || !loaded) return <Loader />
  const { lists: boardLists } = board

  const handleDragEnd = (result) => {
    const { type, source, destination } = result
    switch (type) {
      case 'LIST':
        if (!destination || source.index === destination.index) return

        const newLists = [...boardLists]
        const deletedEl = newLists.splice(source.index, 1)[0]
        newLists.splice(destination.index, 0, deletedEl)

        reorderLists({ id: board.id, newLists })
        break

      case 'CARD':
        if (!destination) return

        if (source.droppableId === destination.droppableId) {
          const listId = result.destination.droppableId
          const newCards = [...lists[listId].cards]
          const deletedEl = newCards.splice(source.index, 1)[0]
          newCards.splice(destination.index, 0, deletedEl)

          reorderCards({ id: listId, newCards })
        } else {
          const listIdToDelete = source.droppableId
          const listIdToPush = destination.droppableId
          const indexToPush = destination.index
          const cardId = result.draggableId

          removeCard(listIdToDelete, cardId)
          pushCard(listIdToPush, cardId, indexToPush)
        }

        break

      default:
        return
    }
  }

  return (
    <Wrapper>
      <Error />
      <Title>{board.title}</Title>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Lists lists={boardLists} boardId={board.id} />
      </DragDropContext>
    </Wrapper>
  )
}

Board.propTypes = {
  board: PropTypes.object,
  lists: PropTypes.object,
  reorderLists: PropTypes.func.isRequired,
  reorderCards: PropTypes.func.isRequired,
  pushCard: PropTypes.func.isRequired,
  removeCard: PropTypes.func.isRequired
}

const Wrapper = styled.div`
  background: #e9f4ff;
  height: 100%;
  padding: 15px;
`

const Title = styled.h3`
  margin-top: 0;
  margin-bottom: 25px;
  text-transform: uppercase;
  font-size: 20px;
  border-bottom: 1px solid #fff;
  padding-bottom: 10px;
  color: #4b5668;
`

export default connect(
  (state, props) => ({
    board: boardSelector(state, props.match.params.id),
    lists: listsSelector(state),
    loading: state.boards.loading,
    loaded: state.boards.loaded
  }),
  { reorderLists, reorderCards, pushCard, removeCard }
)(Board)
