import React from 'react'
import Boards from '../components/Board/Boards'
import AddBoard from '../components/Board/AddBoard'
import styled from 'styled-components'
import { Container } from '../styles/layout'

const Home = () => {
  return (
    <Wrapper>
      <LeftCol>
        <AddBoard />
      </LeftCol>
      <RightCol>
        <Boards />
      </RightCol>
    </Wrapper>
  )
}

const LeftCol = styled.div`
  flex: 200px 0 0;
  border-right: 1px solid #ddd;
  height: 100%;
  padding: 40px 5px;
`

const RightCol = styled.div`
  flex: 1;
  padding: 40px 5px;
`

const Wrapper = styled(Container)`
  height: 100%;
`

export default Home
