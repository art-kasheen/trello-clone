import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { errorMessageSelector } from '../selectors'
import { deleteError } from '../actions'

const Error = ({ message, deleteError }) => {
  useEffect(() => {
    message &&
      setTimeout(() => {
        deleteError()
      }, 5000)
  }, [])

  if (!message) return null

  return (
    <Container>
      <Message>{message}</Message>
    </Container>
  )
}

Error.propTypes = {
  message: PropTypes.string,
  deleteError: PropTypes.func
}

const Container = styled.div`
  background: #ff0000;
  position: fixed;
  top: 20px;
  right: 20px;
  width: 200px;
  padding: 5px 15px;
`

const Message = styled.div`
  color: #fff;
  font-size: 12px;
`

export default connect(
  (state) => ({
    message: errorMessageSelector(state)
  }),
  { deleteError }
)(Error)
