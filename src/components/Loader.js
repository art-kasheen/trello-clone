import React from 'react'
import styled from 'styled-components'

export default () => {
  return (
    <Container>
      <img
        src="https://media.tenor.com/images/b74a5bb0e2e6b66cd6027d815c97ddef/tenor.gif"
        alt="loading"
      />
    </Container>
  )
}

const Container = styled.div`
  margin: auto;
  width: 30px;
  height: 30px;

  img {
    width: 100%;
    height: 100%;
  }
`
