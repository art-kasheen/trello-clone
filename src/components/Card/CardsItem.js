import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { cardsItemSelector } from '../../selectors'
import styled from 'styled-components'
import { Draggable } from 'react-beautiful-dnd'
import { deleteCard } from '../../actions'

const CardsItem = ({ card, index, listId, deleteCard }) => {
  const handleDelete = () => {
    deleteCard(listId, card.id)
  }

  if (!card) return null

  return (
    <Draggable draggableId={card.id} index={index}>
      {(provided, snapshot) => (
        <Item
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Row>
            <ItemTitle>{card.title}</ItemTitle>
            <DeleteButton onClick={handleDelete}>x</DeleteButton>
          </Row>
        </Item>
      )}
    </Draggable>
  )
}

CardsItem.propTypes = {
  id: PropTypes.string.isRequired,
  card: PropTypes.object,
  index: PropTypes.number.isRequired,
  listId: PropTypes.string.isRequired,
  deleteCard: PropTypes.func.isRequired
}

const Item = styled.div`
  background: #ffffff;
  padding: 7px;
  border-radius: 3px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
  margin-bottom: 10px;
`

const Row = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  :hover {
    button {
      opacity: 1;
    }
  }
`

const ItemTitle = styled.h4`
  margin: 0;
  color: #2e2e2e;
  font-weight: 300;
  font-size: 14px;
`

const DeleteButton = styled.button`
  font-size: 12px;
  font-weight: 700;
  color: #172b4d;
  border: 0;
  background: transparent;
  opacity: 0;
  transition: 0.3s ease;
`

export default connect(
  (state, props) => ({
    card: cardsItemSelector(state, props.id)
  }),
  { deleteCard }
)(CardsItem)
