import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchCardsByList } from '../../actions'
import PropTypes from 'prop-types'
import CardsItem from './CardsItem'
import AddCard from './AddCard'
import styled from 'styled-components'
import { Droppable } from 'react-beautiful-dnd'

const Cards = ({ cards, listId, fetchCardsByList }) => {
  useEffect(() => {
    fetchCardsByList(listId)
  }, [])

  const items =
    cards &&
    cards.map((card, index) => (
      <li key={card}>
        <CardsItem id={card} index={index} listId={listId} />
      </li>
    ))

  return (
    <>
      <Droppable droppableId={listId} type="CARD">
        {(provided, snapshot) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            <List>{items}</List>
            {provided.placeholder}
          </div>
        )}
      </Droppable>
      <AddCard listId={listId} />
    </>
  )
}

Cards.propTypes = {
  cards: PropTypes.array,
  listId: PropTypes.string
}

const List = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  min-height: 32px;
`

export default connect(
  null,
  { fetchCardsByList }
)(Cards)
