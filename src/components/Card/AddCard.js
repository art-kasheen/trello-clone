import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addCard } from '../../actions'
import { Button } from '../../styles/button'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

const AddCard = ({ addCard, listId, match }) => {
  const [title, setTitle] = useState('')
  const [isActive, setActive] = useState(false)
  const boardId = match.params.id

  const handleOpenClick = () => {
    setActive(true)
  }

  const handleCancelClick = () => {
    setActive(false)
  }

  const handleAddClick = () => {
    addCard({ title, listId, boardId })
    setTitle('')
    setActive(false)
  }

  const handleInputChange = (ev) => {
    setTitle(ev.target.value)
  }

  return (
    <div>
      {isActive && (
        <>
          <Input
            type="text"
            value={title}
            onChange={handleInputChange}
            placeholder="Enter Card title..."
          />
          <AddButton onClick={handleAddClick} disabled={!title.length}>
            Add
          </AddButton>
          <CancelButton onClick={handleCancelClick}>X</CancelButton>
        </>
      )}
      {!isActive && (
        <Button onClick={handleOpenClick} width="100%">
          Add Card
        </Button>
      )}
    </div>
  )
}

const Input = styled.textarea`
  display: block;
  width: 100%;
  height: 30px;
  padding: 5px 10px;
  margin-bottom: 4px;
  border-radius: 4px;
  border: 0;
  font-size: 14px;
  min-height: 50px;
  resize: none;

  :focus {
    outline: 0;
    box-shadow: 0 0 0 2px rgba(0, 0, 0, 0.1);
  }
`

const AddButton = styled.button`
  height: 35px;
  padding: 0 30px;
  border: 0;
  border-radius: 4px;
  font-size: 14px;
  margin-right: 4px;
`

const CancelButton = styled.button`
  height: 35px;
  padding: 0 30px;
  border: 0;
  border-radius: 4px;
  font-size: 14px;
`

AddCard.propTypes = {
  addCard: PropTypes.func.isRequired,
  listId: PropTypes.string.isRequired
}

const AddCardWithRouter = withRouter(AddCard)

export default connect(
  null,
  { addCard }
)(AddCardWithRouter)
