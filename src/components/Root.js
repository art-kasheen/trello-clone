import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router'
import { connect } from 'react-redux'
import { fetchBoards } from '../actions'
import Header from '../components/Header'
import Home from '../routes/Home'
import Board from '../routes/Board'
import styled from 'styled-components'

const Root = ({ fetchBoards }) => {
  useEffect(() => {
    fetchBoards()
  }, [])

  return (
    <Wrapper>
      <Header />
      <Main>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/board/:id" component={Board} />
        </Switch>
      </Main>
    </Wrapper>
  )
}

Root.propTypes = {
  fetchBoards: PropTypes.func.isRequired
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`

const Main = styled.div`
  flex: 1;
`

export default connect(
  null,
  { fetchBoards }
)(Root)
