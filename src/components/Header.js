import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'

export default () => {
  return (
    <Header>
      <Link to="/">
        <FontAwesomeIcon icon={faHome} />
      </Link>
      <Title>Trello clone</Title>
    </Header>
  )
}

const Title = styled.h1`
  margin: 0 0 0 10px;
  color: #fff;
  font-size: 30px;
  font-weight: 900;
`

const Header = styled.div`
  padding: 5px 15px;
  background: #0394ae;
  display: flex;
  align-items: center;

  a {
    text-decoration: none;
    color: #ffffff;
    font-size: 22px;
  }
`
