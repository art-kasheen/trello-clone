import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { listItemSelector } from '../../selectors'
import Cards from '../Card/Cards'
import styled from 'styled-components'
import { Draggable } from 'react-beautiful-dnd'
import { deleteList } from '../../actions'

const ListsItem = ({ id, index, list, deleteList, boardId }) => {
  const handleDelete = () => {
    deleteList(boardId, id)
  }

  if (!list) return null

  return (
    <Draggable draggableId={`draggable-${id}`} index={index}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Item>
            <Header>
              <ItemTitle>{list.title}</ItemTitle>
              <DeleteButton onClick={handleDelete}>x</DeleteButton>
            </Header>
            <Cards cards={list.cards} listId={id} />
          </Item>
        </div>
      )}
    </Draggable>
  )
}

ListsItem.propTypes = {
  id: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  boardId: PropTypes.string.isRequired,
  list: PropTypes.object,
  deleteList: PropTypes.func.isRequired
}

const Item = styled.div`
  min-height: 100%;
  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
  padding: 10px 7px;
  background: #f1f2f4;
  width: 270px;
  margin-right: 10px;
`

const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 12px;
`

const ItemTitle = styled.h4`
  font-weight: 500;
  font-size: 15px;
  margin: 0;
  padding: 0 8px;
  color: #172b4d;
`

const DeleteButton = styled.button`
  font-size: 12px;
  font-weight: 700;
  color: #172b4d;
  border: 0;
  background: transparent;
`

export default connect(
  (state, props) => ({
    list: listItemSelector(state, props.id)
  }),
  { deleteList }
)(ListsItem)
