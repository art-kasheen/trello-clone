import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchListsByBoardId } from '../../actions'
import PropTypes from 'prop-types'
import ListItem from './ListsItem'
import AddList from './AddList'
import styled from 'styled-components'
import { Droppable } from 'react-beautiful-dnd'

const Lists = ({ lists, boardId, fetchListsByBoardId }) => {
  useEffect(() => {
    fetchListsByBoardId(boardId)
  })

  const items =
    lists &&
    lists.map((id, index) => (
      <li key={id}>
        <ListItem id={id} index={index} boardId={boardId} />
      </li>
    ))

  return (
    <Droppable droppableId="droppable-1" type="LIST" direction="horizontal">
      {(provided, snapshot) => (
        <div ref={provided.innerRef} {...provided.droppableProps}>
          <Wrapper>
            <List>{items}</List>
            {!snapshot.isDraggingOver && <AddList boardId={boardId} />}
          </Wrapper>
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  )
}

Lists.propTypes = {
  lists: PropTypes.array,
  boardId: PropTypes.string
}

const Wrapper = styled.div`
  display: flex;
  overflow-x: scroll;
  padding: 20px 10px 40px;
  margin: 0 -10px;
`

const List = styled.ul`
  display: flex;
  list-style-type: none;
  margin: 0;
  padding: 0;
`

export default connect(
  null,
  { fetchListsByBoardId }
)(Lists)
