import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addList } from '../../actions'
import { Button } from '../../styles/button'
import styled from 'styled-components'

const AddList = ({ addList, boardId }) => {
  const [isActive, setActive] = useState(false)
  const [title, setTitle] = useState('')

  const handleOpenClick = () => {
    setActive(true)
  }

  const handleCancelClick = () => {
    setActive(false)
  }

  const handleAddClick = () => {
    addList({ title, boardId })
    setTitle('')
    setActive(false)
  }

  const handleInputChange = (ev) => {
    setTitle(ev.target.value)
  }

  return (
    <Container>
      {isActive && (
        <div>
          <Input
            type="text"
            value={title}
            onChange={handleInputChange}
            placeholder="Enter list title..."
          />
          <AddButton onClick={handleAddClick} disabled={!title.length}>
            Add
          </AddButton>
          <CancelButton onClick={handleCancelClick}>X</CancelButton>
        </div>
      )}
      {!isActive && (
        <Button width="100%" dark onClick={handleOpenClick}>
          Add List
        </Button>
      )}
    </Container>
  )
}

AddList.propTypes = {
  addList: PropTypes.func,
  boardId: PropTypes.string
}

const Container = styled.div`
  width: 270px;
  flex-shrink: 0;
  padding: 5px;
  border-radius: 4px;
`

const Input = styled.input`
  display: block;
  width: 100%;
  height: 30px;
  padding: 5px 10px;
  margin-bottom: 4px;
  border-radius: 4px;
  border: 0;
  font-size: 14px;

  :focus {
    outline: 0;
    box-shadow: 0 0 0 2px rgba(0, 0, 0, 0.1);
  }
`

const AddButton = styled.button`
  height: 35px;
  padding: 0 30px;
  border: 0;
  border-radius: 4px;
  font-size: 14px;
  margin-right: 4px;
`

const CancelButton = styled.button`
  height: 35px;
  padding: 0 30px;
  border: 0;
  border-radius: 4px;
  font-size: 14px;
`

export default connect(
  null,
  { addList }
)(AddList)
