import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { deleteBoard } from '../../actions'

const BoardsItem = ({ board, deleteBoard }) => {
  const handleDelete = () => {
    deleteBoard(board.id)
  }

  return (
    <Board>
      <Link to={`/board/${board.id}`}>
        <h2>{board.title}</h2>
      </Link>
      <DeleteButton onClick={handleDelete}>x</DeleteButton>
    </Board>
  )
}

BoardsItem.propTypes = {
  board: PropTypes.object.isRequired,
  deleteBoard: PropTypes.func.isRequired
}

const Board = styled.div`
  border-radius: 8px;
  background: #40a35b;
  transition: 0.3s ease;
  position: relative;

  a {
    display: block;
    height: 70px;
    text-decoration: none;
    padding: 10px;
  }

  h2 {
    color: #fff;
    font-size: 18px;
    margin: 0;
  }

  :hover {
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);

    button {
      opacity: 1;
      visibility: visible;
    }
  }
`

const DeleteButton = styled.button`
  background: #fff;
  border: 0;
  color: #000;
  position: absolute;
  top: 5px;
  right: 5px;
  cursor: pointer;
  height: 20px;
  width: 20px;
  line-height: 20px;
  border-radius: 6px;
  padding: 0;
  z-index: 10;
  opacity: 0;
  visibility: hidden;
  transition: 0.3s ease;
`

export default connect(
  null,
  { deleteBoard }
)(BoardsItem)
