import React from 'react'
import PropTypes from 'prop-types'
import BoardsItem from './BoardsItem'
import Loader from '../Loader'
import { connect } from 'react-redux'
import { boardsListSelector } from '../../selectors'
import { fetchBoards } from '../../actions'
import styled from 'styled-components'

const BoardsList = ({ boards, loading, loaded }) => {
  if (loading || !loaded) return <Loader />

  const list =
    boards &&
    boards.map((board) => (
      <ListItem key={board.id}>
        <BoardsItem board={board} />
      </ListItem>
    ))

  return boards ? <List>{list}</List> : <p>No boards yet...</p>
}

BoardsList.propTypes = {
  boards: PropTypes.array,
  loading: PropTypes.bool.isRequired,
  fetchBoards: PropTypes.func.isRequired
}

const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  list-style-type: none;
  margin: 0;
  padding: 0;
`

const ListItem = styled.li`
  flex: 0 0 calc(50% - 10px);
  margin: 0 5px 10px;

  @media screen and (max-width: 600px) {
    flex: 0 0 100%;
  }
`

export default connect(
  (state) => ({
    boards: boardsListSelector(state),
    loading: state.boards.loading,
    loaded: state.boards.loaded
  }),
  { fetchBoards }
)(BoardsList)
