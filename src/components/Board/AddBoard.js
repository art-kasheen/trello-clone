import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addBoard } from '../../actions'
import styled from 'styled-components'
import { Button } from '../../styles/button'

const AddBoard = ({ addBoard }) => {
  const [newBoard, setNewBoard] = useState({ title: '' })

  const handleAdd = () => {
    addBoard(newBoard)
    setNewBoard({ title: '' })
  }

  const handleInputChange = (ev) => {
    setNewBoard({ ...newBoard, title: ev.target.value })
  }

  return (
    <div>
      <Title>Create new board</Title>

      <Label htmlFor="board-title">Add board title</Label>
      <Input
        id="board-title"
        name="board-title"
        type="text"
        value={newBoard.title}
        onChange={handleInputChange}
      />
      <Button fullWidth onClick={handleAdd} disabled={!newBoard.title.length}>
        Add
      </Button>
    </div>
  )
}

AddBoard.propTypes = {
  addBoard: PropTypes.func.isRequired
}

const Title = styled.h3`
  font-size: 16px;
  font-weight: 600;
  margin: 0 0 15px;
  color: #4b5668;
`

const Label = styled.label`
  display: block;
  font-size: 12px;
  color: #aeaeae;
  margin-bottom: 4px;
`

const Input = styled.input`
  display: block;
  border: 1px solid #e4e4e4;
  height: 27px;
  padding: 4px;
  min-width: 100%;
  margin-bottom: 10px;
  transition: 0.3s ease;
  font-size: 14px;

  :focus {
    outline: 0;
    border-color: #b4b4b4;
  }
`

export default connect(
  null,
  { addBoard }
)(AddBoard)
