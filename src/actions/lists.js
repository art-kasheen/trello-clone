import {
  ADD_LIST_REQUEST,
  ADD_LIST_SUCCESS,
  FETCH_LISTS_REQUEST,
  FETCH_LISTS_SUCCESS,
  DELETE_LIST,
  REORDER_LISTS,
  SET_ERROR
} from '../constants'
import { database } from 'firebase'
import uid from 'uid'

export function fetchListsByBoardId(boardId) {
  return (dispatch) => {
    dispatch({
      type: FETCH_LISTS_REQUEST
    })

    database()
      .ref('/lists')
      .orderByChild('boardId')
      .equalTo(boardId)
      .once('value')
      .then((data) => {
        dispatch({
          type: FETCH_LISTS_SUCCESS,
          payload: data.val()
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function addList(list) {
  return (dispatch) => {
    const newList = { ...list, id: uid(), cards: [] }
    const listsRef = database().ref(`/boards/${list.boardId}/lists`)

    dispatch({
      type: ADD_LIST_REQUEST
    })

    database()
      .ref(`/lists/${newList.id}`)
      .set(newList)
      .then(() => listsRef.once('value'))
      .then((data) => {
        const value = data.val()

        return listsRef.set(value ? [...value, newList.id] : [newList.id])
      })
      .then(() =>
        dispatch({
          type: ADD_LIST_SUCCESS,
          payload: newList
        })
      )
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function deleteList(boardId, id) {
  return (dispatch) => {
    const listsRef = database().ref(`/boards/${boardId}/lists`)
    const cardsOrderByListRef = database()
      .ref(`/cards`)
      .orderByChild('listId')
      .equalTo(id)

    database()
      .ref(`/lists/${id}`)
      .remove()
      .then(() => listsRef.once('value'))
      .then((data) => {
        const newData = [...data.val()]
        newData.splice(newData.indexOf(id), 1)

        return listsRef.set(newData)
      })
      .then(() => cardsOrderByListRef.once('value'))
      .then((data) => {
        let updates = {}
        data.forEach((child) => (updates[child.key] = null))

        return database()
          .ref('/cards')
          .update(updates)
      })
      .then(() =>
        dispatch({
          type: DELETE_LIST,
          payload: { boardId, id }
        })
      )
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function reorderLists(lists) {
  return (dispatch) => {
    dispatch({
      type: REORDER_LISTS,
      payload: lists
    })

    database()
      .ref(`/boards/${lists.id}/lists`)
      .set(lists.newLists)
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}
