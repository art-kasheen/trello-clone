import { DELETE_ERROR } from '../constants'

export function deleteError() {
  return {
    type: DELETE_ERROR
  }
}
