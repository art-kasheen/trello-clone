import {
  ADD_CARD_SUCCESS,
  REORDER_CARDS,
  PUSH_CARD,
  REMOVE_CARD,
  DELETE_CARD,
  FETCH_CARDS_REQUEST,
  FETCH_CARDS_SUCCESS,
  SET_ERROR
} from '../constants'
import { database } from 'firebase'
import uid from 'uid'

export function fetchCardsByList(listId) {
  return (dispatch) => {
    dispatch({
      type: FETCH_CARDS_REQUEST
    })

    database()
      .ref('/cards')
      .orderByChild('listId')
      .equalTo(listId)
      .once('value')
      .then((data) => {
        dispatch({
          type: FETCH_CARDS_SUCCESS,
          payload: data.val()
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function reorderCards(cards) {
  return (dispatch) => {
    dispatch({
      type: REORDER_CARDS,
      payload: cards
    })
    database()
      .ref(`/lists/${cards.id}/cards`)
      .set(cards.newCards)
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function pushCard(listId, cardId, index) {
  return (dispatch) => {
    const listsRef = database().ref(`/lists/${listId}/cards`)

    dispatch({
      type: PUSH_CARD,
      payload: { listId, cardId, index }
    })

    listsRef
      .once('value')
      .then((data) => {
        return data.val()
      })
      .then((data) => {
        const newData = data ? [...data] : []
        newData.splice(index, 0, cardId)
        listsRef.set(newData)
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function removeCard(listId, cardId) {
  return (dispatch) => {
    const listsRef = database().ref(`/lists/${listId}/cards`)

    dispatch({
      type: REMOVE_CARD,
      payload: { listId, cardId }
    })

    listsRef
      .once('value')
      .then((data) => {
        const newData = [...data.val()]
        newData.splice(newData.indexOf(cardId), 1)

        listsRef.set(newData)
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function addCard(card) {
  const newCard = { ...card, id: uid() }

  return (dispatch) => {
    const carsdRef = database().ref(`/lists/${card.listId}/cards`)

    database()
      .ref(`/cards/${newCard.id}`)
      .set(newCard)
      .then(() => {
        return carsdRef.once('value')
      })
      .then((data) => {
        const value = data.val()

        return carsdRef.set(value ? [...value, newCard.id] : [newCard.id])
      })
      .then(() => {
        dispatch({
          type: ADD_CARD_SUCCESS,
          payload: newCard
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function deleteCard(listId, id) {
  return (dispatch) => {
    const cardsRef = database().ref(`/lists/${listId}/cards`)

    database()
      .ref(`/cards/${id}`)
      .remove()
      .then(() => {
        return cardsRef.once('value')
      })
      .then((data) => {
        const newData = [...data.val()]
        newData.splice(newData.indexOf(id), 1)

        return cardsRef.set(newData)
      })
      .then(() => {
        dispatch({
          type: DELETE_CARD,
          payload: { listId, id }
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}
