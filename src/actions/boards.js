import {
  ADD_BOARD_REQUEST,
  ADD_BOARD_SUCCESS,
  DELETE_BOARD_SUCCESS,
  FETCH_BOARDS_REQUEST,
  FETCH_BOARDS_SUCCESS,
  SET_ERROR
} from '../constants'

import { database } from 'firebase'
import uid from 'uid'

export function fetchBoards() {
  return (dispatch) => {
    dispatch({
      type: FETCH_BOARDS_REQUEST
    })

    database()
      .ref('/boards')
      .once('value')
      .then((result) => {
        dispatch({
          type: FETCH_BOARDS_SUCCESS,
          payload: result.val()
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function addBoard(board) {
  return (dispatch) => {
    const newBoard = { ...board, id: uid(), lists: [] }

    dispatch({
      type: ADD_BOARD_REQUEST
    })

    database()
      .ref(`/boards/${newBoard.id}`)
      .set(newBoard)
      .then(() => {
        dispatch({
          type: ADD_BOARD_SUCCESS,
          payload: { ...newBoard }
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}

export function deleteBoard(boardId) {
  return (dispatch) => {
    const listsRef = database().ref('/lists')
    const listsOrderByBoardsRef = database()
      .ref(`/lists`)
      .orderByChild('boardId')
      .equalTo(boardId)
      .once('value')
    const cardsRef = database().ref('/cards')
    const cardsOrderByBoardsRef = database()
      .ref(`/cards`)
      .orderByChild('boardId')
      .equalTo(boardId)
      .once('value')

    database()
      .ref(`/boards/${boardId}`)
      .remove()
      .then(() => {
        return listsOrderByBoardsRef
      })
      .then((data) => {
        let updates = {}
        data.forEach((child) => (updates[child.key] = null))

        return listsRef.update(updates)
      })
      .then(() => {
        return cardsOrderByBoardsRef
      })
      .then((data) => {
        let updates = {}
        data.forEach((child) => (updates[child.key] = null))

        return cardsRef.update(updates)
      })
      .then(() => {
        dispatch({
          type: DELETE_BOARD_SUCCESS,
          payload: { id: boardId }
        })
      })
      .catch((error) => {
        dispatch({
          type: SET_ERROR,
          error
        })
      })
  }
}
