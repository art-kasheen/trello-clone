import uid from 'uid'

export const generateId = (store) => (next) => (action) => {
  if (action.generateId && action.generateId === true) {
    next({ id: uid(), ...action })
  } else {
    next(action)
  }
}
