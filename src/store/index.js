import { createStore, applyMiddleware } from 'redux'
import reducer from '../reducer'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { generateId } from '../middlewares/generateId'

const store = createStore(reducer, applyMiddleware(generateId, thunk, logger))

export default store
