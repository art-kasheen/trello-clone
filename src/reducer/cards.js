import produce from 'immer'
import {
  FETCH_CARDS_SUCCESS,
  ADD_CARD_SUCCESS,
  DELETE_CARD
} from '../constants'

const initialState = {
  entities: {},
  loading: false
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case FETCH_CARDS_SUCCESS:
        draft.entities = { ...draft.entities, ...payload }
        break

      case ADD_CARD_SUCCESS:
        draft.entities[payload.id] = { ...payload, id: payload.id }
        break

      case DELETE_CARD:
        delete draft.entities[payload.id]
        break

      default:
        return draft
    }
  })
}
