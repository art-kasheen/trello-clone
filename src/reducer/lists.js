import produce from 'immer'
import {
  ADD_CARD_SUCCESS,
  REORDER_CARDS,
  PUSH_CARD,
  REMOVE_CARD,
  DELETE_CARD,
  DELETE_LIST,
  FETCH_LISTS_REQUEST,
  FETCH_LISTS_SUCCESS,
  ADD_LIST_REQUEST,
  ADD_LIST_SUCCESS
} from '../constants'

const initialState = {
  entities: {},
  loading: false,
  error: ''
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case FETCH_LISTS_REQUEST:
      case ADD_LIST_REQUEST:
        draft.loading = true
        break

      case FETCH_LISTS_SUCCESS:
        draft.entities = { ...draft.entities, ...payload }
        draft.loading = false
        break

      case ADD_LIST_SUCCESS:
        draft.entities[payload.id] = payload
        draft.loading = false
        break

      case DELETE_LIST:
        delete draft.entities[payload.id]
        break

      case ADD_CARD_SUCCESS:
        draft.entities[payload.listId].cards
          ? draft.entities[payload.listId].cards.push(payload.id)
          : (draft.entities[payload.listId].cards = [payload.id])
        break

      case DELETE_CARD:
        draft.entities[payload.listId].cards.splice(
          draft.entities[payload.listId].cards.indexOf(payload.id),
          1
        )
        break

      case REORDER_CARDS:
        draft.entities[payload.id].cards = payload.newCards
        break

      case PUSH_CARD:
        draft.entities[payload.listId].cards !== undefined
          ? draft.entities[payload.listId].cards.splice(
              payload.index,
              0,
              payload.cardId
            )
          : (draft.entities[payload.listId].cards = [payload.cardId])
        break

      case REMOVE_CARD:
        draft.entities[payload.listId].cards.splice(
          draft.entities[payload.listId].cards.indexOf(payload.cardId),
          1
        )
        break

      default:
        return draft
    }
  })
}
