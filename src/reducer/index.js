import { combineReducers } from 'redux'
import boards from './boards'
import lists from './lists'
import cards from './cards'
import error from './error'

export default combineReducers({
  boards,
  lists,
  cards,
  error
})
