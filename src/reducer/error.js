import { SET_ERROR, DELETE_ERROR } from '../constants'
import produce from 'immer'

const initialState = {
  message: ''
}

export default (state = initialState, action) => {
  const { type } = action
  return produce(state, (draft) => {
    switch (type) {
      case SET_ERROR:
        draft.message = action.error.message
        break

      case DELETE_ERROR:
        draft.message = ''
        break

      default:
        return draft
    }
  })
}
