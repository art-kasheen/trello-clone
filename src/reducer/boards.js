import produce from 'immer'
import {
  FETCH_BOARDS_REQUEST,
  FETCH_BOARDS_SUCCESS,
  ADD_BOARD_REQUEST,
  ADD_BOARD_SUCCESS,
  DELETE_BOARD_SUCCESS,
  ADD_LIST_SUCCESS,
  DELETE_LIST,
  REORDER_LISTS
} from '../constants'

const initialState = {
  entities: {},
  loading: false,
  loaded: false,
  error: ''
}

export default (state = initialState, action) => {
  const { type, payload } = action

  return produce(state, (draft) => {
    switch (type) {
      case FETCH_BOARDS_REQUEST:
      case ADD_BOARD_REQUEST:
        draft.loading = true
        break

      case FETCH_BOARDS_SUCCESS:
        draft.loading = false
        draft.entities = payload ? payload : {}
        draft.loaded = true
        break

      case ADD_BOARD_SUCCESS:
        draft.loading = false
        draft.entities[payload.id] = payload
        break

      case DELETE_BOARD_SUCCESS:
        delete draft.entities[payload.id]
        break

      case ADD_LIST_SUCCESS:
        draft.entities[payload.boardId].lists.push(payload.id)
        break

      case REORDER_LISTS:
        draft.entities[payload.id].lists = payload.newLists
        break

      case DELETE_LIST:
        draft.entities[payload.boardId].lists.splice(
          draft.entities[payload.boardId].lists.indexOf(payload.id),
          1
        )
        break

      default:
        return draft
    }
  })
}
