import React from 'react'
import Root from './components/Root'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import store from './store'
import { GlobalStyle } from './styles/global'

function App() {
  return (
    <Provider store={store}>
      <GlobalStyle />
      <Router>
        <Root />
      </Router>
    </Provider>
  )
}

export default App
