import { createSelector } from 'reselect'

export const boardsSelector = (state) => state.boards.entities
export const listsSelector = (state) => state.lists.entities
export const cardsSelector = (state) => state.cards.entities
export const errorSelector = (state) => state.error
export const idSelector = (state, id) => id

export const boardsListSelector = createSelector(
  boardsSelector,
  (boards) => (Object.keys(boards).length ? Object.values(boards) : null)
)

export const boardSelector = createSelector(
  boardsSelector,
  idSelector,
  (boards, id) => boards[id]
)

export const listItemSelector = createSelector(
  listsSelector,
  idSelector,
  (lists, id) => lists[id]
)

export const cardsItemSelector = createSelector(
  cardsSelector,
  idSelector,
  (cards, id) => cards[id]
)

export const errorMessageSelector = createSelector(
  errorSelector,
  (error) => error.message
)
